/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <gtest/gtest.h>
#include <memory>
#include <set>

#include <trexe_cmdlinespec.h>
#include <trexe_executionoptions.h>

#include <buildboxcommon_commandline.h>
#include <buildboxcommon_connectionoptions.h>

using namespace trexe;
using namespace buildboxcommon;

TEST(TrexeExecutionOptionsTest, TestDefaultConstructor)
{
    ExecutionOptions execOptions();
}

TEST(TrexeExecutionOptionsTest, TestParseFromCmdLine)
{
    CmdLineSpec trexeSpec(
        buildboxcommon::ConnectionOptionsCommandLine("", "", true),
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-", false),
        buildboxcommon::ConnectionOptionsCommandLine("ActionCache", "ac-",
                                                     false),
        buildboxcommon::ConnectionOptionsCommandLine("Execution", "exec-",
                                                     false));

    const char *argv[] = {"./path/to/trexe",
                          "--remote=https://127.0.0.1:1234",
                          "--d=/test/download/dir",
                          "--working-dir=/path/to/working-dir",
                          "--input-path=/path/to/input-path-1",
                          "--input-path=/path/to/input-path-2",
                          "--output-path=/output/path-1",
                          "--output-path=/output/path-2",
                          "--platform-properties=P1=V1,P2=V2",
                          "--environment=E1=V3,E2=V4",
                          "--exec-timeout=1",
                          "--skip-cache-lookup",
                          "--do-not-cache",
                          "--priority=1",
                          "--correlated-invocations-id=corr",
                          "--tool-invocation-id=tid",
                          "--tool-name=tname",
                          "--tool-version=tver",
                          "--no-wait",
                          "--result-metadata-file=/tmp/result.json",
                          "/usr/bin/echo"};
    int argc = sizeof(argv) / sizeof(const char *);

    CommandLine commandLine(trexeSpec.d_spec);
    const bool success = commandLine.parse(argc, argv);
    ASSERT_EQ(true, success);

    auto parsedOptions =
        ExecutionOptions::fromCommandLine({trexeSpec.d_command}, commandLine);
    ASSERT_EQ(false, parsedOptions.d_blockingExecute);
    ASSERT_EQ(true, parsedOptions.d_skipCacheLookup);
    ASSERT_EQ("/test/download/dir", parsedOptions.d_downloadResultsPath);
    ASSERT_EQ(1, parsedOptions.d_argv.size());
    ASSERT_EQ("/usr/bin/echo", parsedOptions.d_argv[0]);
    ASSERT_EQ("/path/to/working-dir", parsedOptions.d_workingDir);

    auto inputPaths = parsedOptions.d_inputPaths;
    ASSERT_EQ(2, inputPaths.size());
    ASSERT_NE(inputPaths.end(), find(inputPaths.begin(), inputPaths.end(),
                                     "/path/to/input-path-1"));
    ASSERT_NE(inputPaths.end(), find(inputPaths.begin(), inputPaths.end(),
                                     "/path/to/input-path-2"));

    auto outputPaths = parsedOptions.d_outputPaths;
    ASSERT_EQ(2, outputPaths.size());
    ASSERT_NE(outputPaths.end(),
              find(outputPaths.begin(), outputPaths.end(), "/output/path-1"));
    ASSERT_NE(outputPaths.end(),
              find(outputPaths.begin(), outputPaths.end(), "/output/path-2"));

    std::vector<std::pair<std::string, std::string>> v1 = {
        std::make_pair("P1", "V1"), std::make_pair("P2", "V2")};
    std::vector<std::pair<std::string, std::string>> v2 = {
        std::make_pair("E1", "V3"), std::make_pair("E2", "V4")};
    ASSERT_EQ(v1, parsedOptions.d_platform);
    ASSERT_EQ(v2, parsedOptions.d_environment);

    ASSERT_EQ(1, parsedOptions.d_execTimeout);
    ASSERT_EQ(true, parsedOptions.d_doNotCache);

    ASSERT_EQ("corr",
              parsedOptions.d_metadata.at("correlated-invocations-id"));
    ASSERT_EQ("tid", parsedOptions.d_metadata.at("tool-invocation-id"));
    ASSERT_EQ(std::string("trexe") + std::string(":") + std::string("tname"),
              parsedOptions.d_metadata.at("tool-name"));

#ifndef TREXE_VERSION
#error "TREXE_VERSION is not defined"
#else
    const std::string TREXE_VERSION_EO = TREXE_VERSION; // set by CMake
#endif
    ASSERT_EQ(TREXE_VERSION_EO + std::string(":") + std::string("tver"),
              parsedOptions.d_metadata.at("tool-version"));

    ASSERT_EQ(1, *(parsedOptions.d_priority));
    ASSERT_EQ("/tmp/result.json", parsedOptions.d_resultMetadataFile);
}

TEST(TrexeExecutionOptionsTest, TestParseFromCmdLineGetCompleted)
{
    CmdLineSpec trexeSpec(
        buildboxcommon::ConnectionOptionsCommandLine("", "", true),
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-", false),
        buildboxcommon::ConnectionOptionsCommandLine("ActionCache", "ac-",
                                                     false),
        buildboxcommon::ConnectionOptionsCommandLine("Execution", "exec-",
                                                     false));

    const char *argv[] = {"./path/to/trexe",
                          "--remote=https://127.0.0.1:1234",
                          "--d=/test/download/dir",
                          "--correlated-invocations-id=corr",
                          "--tool-invocation-id=tid",
                          "--tool-name=tname",
                          "--tool-version=tver",
                          "--operation=opID",
                          "--result-metadata-file=/tmp/result.json"};
    int argc = sizeof(argv) / sizeof(const char *);

    CommandLine commandLine(trexeSpec.d_spec);
    const bool success = commandLine.parse(argc, argv);
    ASSERT_EQ(true, success);

    auto parsedOptions =
        ExecutionOptions::fromCommandLine({trexeSpec.d_command}, commandLine);

    ASSERT_EQ("corr",
              parsedOptions.d_metadata.at("correlated-invocations-id"));
    ASSERT_EQ("tid", parsedOptions.d_metadata.at("tool-invocation-id"));
    ASSERT_EQ(std::string("trexe") + std::string(":") + std::string("tname"),
              parsedOptions.d_metadata.at("tool-name"));

#ifndef TREXE_VERSION
#error "TREXE_VERSION is not defined"
#else
    const std::string TREXE_VERSION_EO = TREXE_VERSION; // set by CMake
#endif
    ASSERT_EQ(TREXE_VERSION_EO + std::string(":") + std::string("tver"),
              parsedOptions.d_metadata.at("tool-version"));

    ASSERT_EQ("opID", parsedOptions.d_operation);
    ASSERT_EQ("/tmp/result.json", parsedOptions.d_resultMetadataFile);
}

TEST(TrexeExecutionOptionsTest, TestParseFromCmdLineNoOptionals)
{
    CmdLineSpec trexeSpec(
        buildboxcommon::ConnectionOptionsCommandLine("", "", true),
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-", false),
        buildboxcommon::ConnectionOptionsCommandLine("ActionCache", "ac-",
                                                     false),
        buildboxcommon::ConnectionOptionsCommandLine("Execution", "exec-",
                                                     false));

    const char *argv[] = {"./path/to/trexe",
                          "--remote=https://127.0.0.1:1234",
                          "--d=/test/download/dir",
                          "--working-dir=/path/to/working-dir",
                          "--input-path=/path/to/input-path-1",
                          "--input-path=/path/to/input-path-2",
                          "--output-path=/output/path-1",
                          "--output-path=/output/path-2",
                          "--result-metadata-file=/tmp/result.json",
                          "/usr/bin/echo"};
    int argc = sizeof(argv) / sizeof(const char *);

    CommandLine commandLine(trexeSpec.d_spec);
    const bool success = commandLine.parse(argc, argv);
    ASSERT_EQ(true, success);

    auto parsedOptions =
        ExecutionOptions::fromCommandLine(trexeSpec.d_command, commandLine);
    ASSERT_EQ(true, parsedOptions.d_blockingExecute);
    ASSERT_EQ(false, parsedOptions.d_skipCacheLookup);
    ASSERT_EQ("/test/download/dir", parsedOptions.d_downloadResultsPath);
    ASSERT_EQ(1, parsedOptions.d_argv.size());
    ASSERT_EQ("/usr/bin/echo", parsedOptions.d_argv[0]);
    ASSERT_EQ("/path/to/working-dir", parsedOptions.d_workingDir);

    auto inputPaths = parsedOptions.d_inputPaths;
    ASSERT_EQ(2, inputPaths.size());
    ASSERT_NE(inputPaths.end(), find(inputPaths.begin(), inputPaths.end(),
                                     "/path/to/input-path-1"));
    ASSERT_NE(inputPaths.end(), find(inputPaths.begin(), inputPaths.end(),
                                     "/path/to/input-path-2"));

    auto outputPaths = parsedOptions.d_outputPaths;
    ASSERT_EQ(2, outputPaths.size());
    ASSERT_NE(outputPaths.end(),
              find(outputPaths.begin(), outputPaths.end(), "/output/path-1"));
    ASSERT_NE(outputPaths.end(),
              find(outputPaths.begin(), outputPaths.end(), "/output/path-2"));

    std::vector<std::pair<std::string, std::string>> v1 = {};
    std::vector<std::pair<std::string, std::string>> v2 = {};
    ASSERT_EQ(v1, parsedOptions.d_platform);
    ASSERT_EQ(v2, parsedOptions.d_environment);

    ASSERT_EQ(0, parsedOptions.d_execTimeout);
    ASSERT_EQ(false, parsedOptions.d_doNotCache);

    ASSERT_EQ(nullptr, parsedOptions.d_priority);

    ASSERT_EQ("", parsedOptions.d_metadata.at("correlated-invocations-id"));
    ASSERT_EQ("", parsedOptions.d_metadata.at("tool-invocation-id"));
    ASSERT_EQ("trexe", parsedOptions.d_metadata.at("tool-name"));

#ifndef TREXE_VERSION
#error "TREXE_VERSION is not defined"
#else
    const std::string TREXE_VERSION_EO = TREXE_VERSION; // set by CMake
#endif
    ASSERT_EQ(TREXE_VERSION_EO, parsedOptions.d_metadata.at("tool-version"));
    ASSERT_EQ("/tmp/result.json", parsedOptions.d_resultMetadataFile);
}

TEST(TrexeExecutionOptionsTest, TestParseSeparateOptionals)
{
    CmdLineSpec trexeSpec(
        buildboxcommon::ConnectionOptionsCommandLine("", "", false),
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-", false),
        buildboxcommon::ConnectionOptionsCommandLine("ActionCache", "ac-",
                                                     false),
        buildboxcommon::ConnectionOptionsCommandLine("Execution", "exec-",
                                                     false));

    const char *argv[] = {"./path/to/trexe",
                          "--remote=https://127.0.0.1:1111",
                          "--client-cert",
                          "XYZ",
                          "--token-reload-interval=10",
                          "--cas-token-reload-interval=5",
                          "--ac-remote",
                          "https://127.0.0.1:4444",
                          "--ac-request-timeout=5",
                          "--exec-remote",
                          "https://127.0.0.1:8888",
                          "--exec-retry-limit=8",
                          "/usr/bin/echo"};
    int argc = sizeof(argv) / sizeof(const char *);

    CommandLine commandLine(trexeSpec.d_spec);
    const bool success = commandLine.parse(argc, argv);
    ASSERT_EQ(true, success);

    ConnectionOptions connectionOptions, connectionOptionsEX,
        connectionOptionsCAS, connectionOptionsAC;

    ConnectionOptionsCommandLine::configureChannel(commandLine, "",
                                                   &connectionOptions);

    connectionOptionsCAS = connectionOptions;
    connectionOptionsAC = connectionOptions;
    connectionOptionsEX = connectionOptions;

    ConnectionOptionsCommandLine::updateChannelOptions(commandLine, "cas-",
                                                       &connectionOptionsCAS);
    ConnectionOptionsCommandLine::updateChannelOptions(commandLine, "ac-",
                                                       &connectionOptionsAC);
    ConnectionOptionsCommandLine::updateChannelOptions(commandLine, "exec-",
                                                       &connectionOptionsEX);

    ASSERT_EQ("https://127.0.0.1:1111", connectionOptionsCAS.d_url);
    ASSERT_EQ("https://127.0.0.1:4444", connectionOptionsAC.d_url);
    ASSERT_EQ("https://127.0.0.1:8888", connectionOptionsEX.d_url);
    ASSERT_EQ("XYZ", connectionOptions.d_clientCertPath);
    ASSERT_EQ("XYZ", connectionOptionsEX.d_clientCertPath);
    ASSERT_EQ("XYZ", connectionOptionsCAS.d_clientCertPath);
    ASSERT_EQ("4", connectionOptions.d_retryLimit);
    ASSERT_EQ("8", connectionOptionsEX.d_retryLimit);
    ASSERT_EQ("5", connectionOptionsAC.d_requestTimeout);
    ASSERT_EQ("10", connectionOptionsAC.d_tokenReloadInterval);
    ASSERT_EQ("5", connectionOptionsCAS.d_tokenReloadInterval);
}
