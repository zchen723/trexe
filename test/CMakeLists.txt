find_file(BuildboxGTestSetup.cmake BuildboxGTestSetup.cmake HINTS ${BuildboxCommon_DIR})
include(${BuildboxGTestSetup.cmake})


# Every directory containing a source file needed by the tests, must be added here.
include_directories(. ../trexe/)

# Add any srcs/headers containing code for tests to link against here.
# The reason this isn't necessary yet is because the fixtures directory only includes a header, and there is nothing to link against.
# Once modified, add the lib `extra_libs` to the target_link_libraries in the add_trexe_test macro.
# file(GLOB EXTRA_LIBS_SRCS fixtures/*.cpp fixtures/*.h)
# add_library(extra_libs ${EXTRA_LIBS_SRCS})
# target_include_directories(extra_libs PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/fixtures)
# target_link_libraries(extra_libs casd ${GTEST_TARGET})

# This macro creates the specific test executable, google test and google mock.
# Current limitation is the TEST_SOURCE must only be one file.
# If more than 1 file is needed, combine the sources into a list.
macro(add_trexe_test TEST_NAME TEST_SOURCE)
    # Create a separate test executable per test source.
    add_executable(${TEST_NAME} ${TEST_SOURCE})

    # This allows us to pass an optional argument if the cwd for the test is not the default.
    set(ExtraMacroArgs ${ARGN})
    list(LENGTH ExtraMacroArgs NumExtraArgs)
    if(${NumExtraArgs} GREATER 0)
      list(GET ExtraMacroArgs 0 TEST_WORKING_DIRECTORY)
    else()
      set(TEST_WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    endif()

    add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME} WORKING_DIRECTORY ${TEST_WORKING_DIRECTORY})
    target_link_libraries(${TEST_NAME} PUBLIC
        trexe-lib
        ${GTEST_TARGET}
        ${GMOCK_TARGET}
        ${GTEST_MAIN_TARGET}
    )
endmacro()


add_trexe_test(actionbuilder_tests trexe_actionbuilder.t.cpp)
add_trexe_test(cmdlinespec_tests trexe_cmdlinespec.t.cpp)
add_trexe_test(executionoptions_tests trexe_executionoptions.t.cpp)
add_trexe_test(executioncontext_tests trexe_executioncontext.t.cpp)
