#!/bin/bash

mkdir -p /tmp/upload/input1;

echo "#!/bin/bash
echo \"hello\"
" > /tmp/upload/input1/hello.sh;

chmod +x /tmp/upload/input1/hello.sh;

# The first build
/usr/local/bin/trexe --remote=http://controller:50051 --no-wait \
    --input-path=/tmp/upload/input1 --output-path=output \
    --result-metadata-file=/tmp/result1.json "./hello.sh"

sleep 5;

# The second build should observe the cache hit
OUTPUT=$(/usr/local/bin/trexe --remote=http://controller:50051 --no-wait \
    --input-path=/tmp/upload/input1 --output-path=output \
    --result-metadata-file=/tmp/result2.json "./hello.sh");


echo "Output: ${OUTPUT}"

if [[ "$OUTPUT" != "hello" ]]; then
    exit 1;
fi
