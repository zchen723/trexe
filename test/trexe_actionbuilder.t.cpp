/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <string>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_merklize.h>
#include <buildboxcommon_protos.h>

#include <trexe_actionbuilder.h>

using namespace ::testing;
using namespace trexe;

TEST(ActionBuilderTest, TestEchoNoInputsNoOutputs)
{
    auto actionBuilder = ActionBuilder({"/bin/echo", "hello world"}, ".", {},
                                       {}, {}, {}, 0, false, "", {});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    auto args = command->arguments();
    ASSERT_EQ(2, command->arguments_size());
    ASSERT_EQ("/bin/echo", args[0]);
    ASSERT_EQ("hello world", args[1]);

    ASSERT_EQ(0, command->output_paths_size());

    ASSERT_TRUE(command->environment_variables().empty());
    ASSERT_TRUE(command->platform().properties().empty());

    // Check `Action` proto
    const auto action = actionBuilder.actionProto();
    const auto actionDigest = CASHash::hash(action->SerializeAsString());
    ASSERT_EQ(actionDigest.hash(), actionBuilder.actionDigest()->hash());
    ASSERT_EQ(actionDigest.size_bytes(),
              actionBuilder.actionDigest()->size_bytes());

    ASSERT_EQ(commandDigest.hash(), action->command_digest().hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              action->command_digest().size_bytes());

    // Empty input root
    NestedDirectory emptyDir;
    const auto emptyDirDigest = emptyDir.to_digest();
    ASSERT_EQ(emptyDirDigest.hash(), action->input_root_digest().hash());
    ASSERT_EQ(emptyDirDigest.size_bytes(),
              action->input_root_digest().size_bytes());

    ASSERT_EQ(0, action->timeout().seconds());
    ASSERT_EQ(false, action->do_not_cache());
}

TEST(ActionBuilderTest, TestEchoNoInputsNoOutputsWithEnv)
{
    auto actionBuilder = ActionBuilder(
        {"/bin/echo", "hello world"}, ".", {}, {}, {},
        {std::make_pair("ENV1", "A"), std::make_pair("ENV2", "B")}, 0, false,
        "", {});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    auto args = command->arguments();
    ASSERT_EQ(2, command->arguments_size());
    ASSERT_EQ("/bin/echo", args[0]);
    ASSERT_EQ("hello world", args[1]);

    ASSERT_EQ(0, command->output_paths_size());

    ASSERT_EQ(2, command->environment_variables_size());
    ASSERT_EQ("ENV1", command->environment_variables(0).name());
    ASSERT_EQ("A", command->environment_variables(0).value());
    ASSERT_EQ("ENV2", command->environment_variables(1).name());
    ASSERT_EQ("B", command->environment_variables(1).value());

    ASSERT_TRUE(command->platform().properties().empty());

    // Check `Action` proto
    const auto action = actionBuilder.actionProto();
    const auto actionDigest = CASHash::hash(action->SerializeAsString());
    ASSERT_EQ(actionDigest.hash(), actionBuilder.actionDigest()->hash());
    ASSERT_EQ(actionDigest.size_bytes(),
              actionBuilder.actionDigest()->size_bytes());

    ASSERT_EQ(commandDigest.hash(), action->command_digest().hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              action->command_digest().size_bytes());

    // Empty input root
    NestedDirectory emptyDir;
    const auto emptyDirDigest = emptyDir.to_digest();
    ASSERT_EQ(emptyDirDigest.hash(), action->input_root_digest().hash());
    ASSERT_EQ(emptyDirDigest.size_bytes(),
              action->input_root_digest().size_bytes());

    ASSERT_EQ(0, action->timeout().seconds());
    ASSERT_EQ(false, action->do_not_cache());
}

TEST(ActionBuilderTest, TestEchoNoInputWithOutputs)
{
    auto actionBuilder = ActionBuilder(
        {"/bin/echo", "hello world"}, ".", {},
        {"/path/to/output-1", "/path/to/output-2"}, {}, {}, 0, false, "", {});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    auto args = command->arguments();
    ASSERT_EQ(2, command->arguments_size());
    ASSERT_EQ("/bin/echo", args[0]);
    ASSERT_EQ("hello world", args[1]);

    auto outputs = command->output_paths();
    ASSERT_EQ(2, command->output_paths_size());
    ASSERT_EQ("/path/to/output-1", outputs[0]);
    ASSERT_EQ("/path/to/output-2", outputs[1]);

    ASSERT_TRUE(command->environment_variables().empty());
    ASSERT_TRUE(command->platform().properties().empty());

    // Check `Action` proto
    const auto action = actionBuilder.actionProto();
    const auto actionDigest = CASHash::hash(action->SerializeAsString());
    ASSERT_EQ(actionDigest.hash(), actionBuilder.actionDigest()->hash());
    ASSERT_EQ(actionDigest.size_bytes(),
              actionBuilder.actionDigest()->size_bytes());

    ASSERT_EQ(commandDigest.hash(), action->command_digest().hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              action->command_digest().size_bytes());

    // Empty input root
    NestedDirectory emptyDir;
    const auto emptyDirDigest = emptyDir.to_digest();
    ASSERT_EQ(emptyDirDigest.hash(), action->input_root_digest().hash());
    ASSERT_EQ(emptyDirDigest.size_bytes(),
              action->input_root_digest().size_bytes());

    ASSERT_EQ(0, action->timeout().seconds());
    ASSERT_EQ(false, action->do_not_cache());
}

TEST(ActionBuilderTest, TestEchoNoInputsNoOutputsWithPlatform)
{
    auto actionBuilder = ActionBuilder(
        {"/bin/echo", "hello world"}, ".", {}, {},
        {std::make_pair("PLT1", "A"), std::make_pair("PLT2", "B")}, {}, 0,
        false, "", {});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    auto args = command->arguments();
    ASSERT_EQ(2, command->arguments_size());
    ASSERT_EQ("/bin/echo", args[0]);
    ASSERT_EQ("hello world", args[1]);

    ASSERT_EQ(0, command->output_paths_size());

    ASSERT_TRUE(command->environment_variables().empty());

    ASSERT_EQ(2, command->platform().properties_size());
    ASSERT_EQ("PLT1", command->platform().properties(0).name());
    ASSERT_EQ("A", command->platform().properties(0).value());
    ASSERT_EQ("PLT2", command->platform().properties(1).name());
    ASSERT_EQ("B", command->platform().properties(1).value());

    // Check `Action` proto
    const auto action = actionBuilder.actionProto();
    const auto actionDigest = CASHash::hash(action->SerializeAsString());
    ASSERT_EQ(actionDigest.hash(), actionBuilder.actionDigest()->hash());
    ASSERT_EQ(actionDigest.size_bytes(),
              actionBuilder.actionDigest()->size_bytes());

    ASSERT_EQ(commandDigest.hash(), action->command_digest().hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              action->command_digest().size_bytes());

    // Empty input root
    NestedDirectory emptyDir;
    const auto emptyDirDigest = emptyDir.to_digest();
    ASSERT_EQ(emptyDirDigest.hash(), action->input_root_digest().hash());
    ASSERT_EQ(emptyDirDigest.size_bytes(),
              action->input_root_digest().size_bytes());

    ASSERT_EQ(0, action->timeout().seconds());
    ASSERT_EQ(false, action->do_not_cache());
}

TEST(ActionBuilderTest, TestEchoNoInputsNoOutputsWithTimeout)
{
    auto actionBuilder = ActionBuilder({"/bin/echo", "hello world"}, ".", {},
                                       {}, {}, {}, 1, false, "", {});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    auto args = command->arguments();
    ASSERT_EQ(2, command->arguments_size());
    ASSERT_EQ("/bin/echo", args[0]);
    ASSERT_EQ("hello world", args[1]);

    ASSERT_EQ(0, command->output_paths_size());

    ASSERT_TRUE(command->environment_variables().empty());
    ASSERT_TRUE(command->platform().properties().empty());

    // Check `Action` proto
    const auto action = actionBuilder.actionProto();
    const auto actionDigest = CASHash::hash(action->SerializeAsString());
    ASSERT_EQ(actionDigest.hash(), actionBuilder.actionDigest()->hash());
    ASSERT_EQ(actionDigest.size_bytes(),
              actionBuilder.actionDigest()->size_bytes());

    ASSERT_EQ(commandDigest.hash(), action->command_digest().hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              action->command_digest().size_bytes());

    // Empty input root
    NestedDirectory emptyDir;
    const auto emptyDirDigest = emptyDir.to_digest();
    ASSERT_EQ(emptyDirDigest.hash(), action->input_root_digest().hash());
    ASSERT_EQ(emptyDirDigest.size_bytes(),
              action->input_root_digest().size_bytes());

    ASSERT_EQ(1, action->timeout().seconds());
    ASSERT_EQ(false, action->do_not_cache());
}

TEST(ActionBuilderTest, TestEchoNoInputsNoOutputsDoNotCache)
{
    auto actionBuilder = ActionBuilder({"/bin/echo", "hello world"}, ".", {},
                                       {}, {}, {}, 0, true, "", {});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    auto args = command->arguments();
    ASSERT_EQ(2, command->arguments_size());
    ASSERT_EQ("/bin/echo", args[0]);
    ASSERT_EQ("hello world", args[1]);

    ASSERT_EQ(0, command->output_paths_size());

    ASSERT_TRUE(command->environment_variables().empty());
    ASSERT_TRUE(command->platform().properties().empty());

    // Check `Action` proto
    const auto action = actionBuilder.actionProto();
    const auto actionDigest = CASHash::hash(action->SerializeAsString());
    ASSERT_EQ(actionDigest.hash(), actionBuilder.actionDigest()->hash());
    ASSERT_EQ(actionDigest.size_bytes(),
              actionBuilder.actionDigest()->size_bytes());

    ASSERT_EQ(commandDigest.hash(), action->command_digest().hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              action->command_digest().size_bytes());

    // Empty input root
    NestedDirectory emptyDir;
    const auto emptyDirDigest = emptyDir.to_digest();
    ASSERT_EQ(emptyDirDigest.hash(), action->input_root_digest().hash());
    ASSERT_EQ(emptyDirDigest.size_bytes(),
              action->input_root_digest().size_bytes());

    ASSERT_EQ(0, action->timeout().seconds());
    ASSERT_EQ(true, action->do_not_cache());
}

TEST(ActionBuilderTest, TestCatMultipleInputsNoOutputs)
{
    auto actionBuilder =
        ActionBuilder({"/bin/sh", "-c", "'/bin/cat foo.log'"}, ".",
                      {"./data/input_1", "./data/input_2/subdir"}, {}, {}, {},
                      0, true, "", {});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    auto args = command->arguments();
    ASSERT_EQ(3, command->arguments_size());
    ASSERT_EQ("/bin/sh", args[0]);
    ASSERT_EQ("-c", args[1]);
    ASSERT_EQ("'/bin/cat foo.log'", args[2]);

    ASSERT_EQ(0, command->output_paths_size());

    // Check `Action` proto
    const auto action = actionBuilder.actionProto();
    const auto actionDigest = CASHash::hash(action->SerializeAsString());
    ASSERT_EQ(actionDigest.hash(), actionBuilder.actionDigest()->hash());
    ASSERT_EQ(actionDigest.size_bytes(),
              actionBuilder.actionDigest()->size_bytes());

    ASSERT_EQ(commandDigest.hash(), action->command_digest().hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              action->command_digest().size_bytes());
}

TEST(ActionBuilderTest, TestSetSalt)
{
    auto actionBuilder = ActionBuilder({"/bin/echo", "hello world"}, ".", {},
                                       {}, {}, {}, 0, true, "1234", {});

    const auto action = actionBuilder.actionProto();
    ASSERT_EQ("1234", action->salt());
    ASSERT_NE("", action->salt());
}

TEST(ActionBuilderTest, TestNodeProperties)
{
    auto actionBuilder =
        ActionBuilder({"/bin/echo", "hello world"}, ".", {}, {}, {}, {}, 0,
                      true, "", {"mtime", "unix_mode"});

    // Check `Command` proto
    const auto command = actionBuilder.commandProto();
    const auto commandDigest = CASHash::hash(command->SerializeAsString());
    ASSERT_EQ(commandDigest.hash(), actionBuilder.commandDigest()->hash());
    ASSERT_EQ(commandDigest.size_bytes(),
              actionBuilder.commandDigest()->size_bytes());

    const auto properties = command->output_node_properties();
    ASSERT_EQ(2, command->output_node_properties_size());
    ASSERT_EQ("mtime", properties[0]);
    ASSERT_EQ("unix_mode", properties[1]);
}
