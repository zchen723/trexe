/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_logging.h>

#include <trexe_executionoptions.h>

namespace trexe {

const std::string TREXE_METADATA_TOOL_NAME = "trexe";

#ifndef TREXE_VERSION
#error "TREXE_VERSION is not defined"
#else
const std::string TREXE_VERSION_EO = TREXE_VERSION; // set by CMake
#endif

std::vector<std::pair<std::string, std::string>>
pairStringToVector(const std::string &sourcePairString,
                   const std::string &argName)
{
    std::vector<std::pair<std::string, std::string>> pairVector = {};
    std::string pairString = sourcePairString;
    if (!pairString.empty()) {
        std::istringstream pairStream(pairString);
        while (pairStream) {
            std::string s;
            std::getline(pairStream, s, ',');
            if (s.empty()) { // stream adds an extra blank character we need to
                             // skip
                continue;
            }
            std::size_t splitIndex = s.find("=");
            if (splitIndex == std::string::npos) {
                throw std::runtime_error(
                    std::string("Incorrect ") + s +
                    std::string(
                        " argument format. Please use KEY=VALUE syntax"));
            }
            pairVector.push_back(std::make_pair(s.substr(0, splitIndex),
                                                s.substr(splitIndex + 1)));
        }
    }
    return pairVector;
};

ExecutionOptions
ExecutionOptions::fromCommandLine(const std::vector<std::string> &argv,
                                  const buildboxcommon::CommandLine &cmd)
{
    ExecutionOptions options;

    if (cmd.exists("cancel")) {
        if (!cmd.exists("operation")) {
            throw std::runtime_error("Cancellation requires an operation id");
        }
        options.d_cancelMode = true;
    }

    if (cmd.exists("operation")) {
        if (cmd.getString("working-dir") != ".") {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument working-dir");
        }
        if (cmd.exists("input-path")) {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument input-path");
        }
        if (cmd.exists("output-path")) {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument output-path");
        }
        if (cmd.getString("platform-properties") != "") {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument platform-properties");
        }
        if (cmd.getString("environment") != "") {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument environment");
        }
        if (cmd.getInt("exec-timeout") != 0) {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument exec-timeout");
        }
        if (cmd.getBool("skip-cache-lookup")) {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument skip-cache-lookup");
        }
        if (cmd.getBool("do-not-cache")) {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument do-not-cache");
        }
        if (cmd.exists("priority")) {
            throw std::runtime_error("operation fetching mode does not "
                                     "take the argument priority");
        }
    }

    options.d_downloadResultsPath = cmd.getString("d");

    if (!cmd.exists("operation") && !argv.size()) {
        throw std::runtime_error("No command given.");
    }
    if (cmd.exists("operation") && argv.size()) {
        throw std::runtime_error("Cannot execute a command and fetch an "
                                 "operation at the same time");
    }
    options.d_argv = argv;

    options.d_workingDir = cmd.getString("working-dir");

    const auto inputPaths = cmd.getVS("input-path");
    for (const auto &path : inputPaths) {
        options.d_inputPaths.emplace(path);
    }

    const auto outputPaths = cmd.getVS("output-path");
    for (const auto &path : outputPaths) {
        options.d_outputPaths.emplace(path);
    }

    const auto outputNodeProperties = cmd.getVS("output-node-properties");
    for (const auto &property : outputNodeProperties) {
        options.d_outputNodeProperties.emplace(property);
    }

    options.d_platform = pairStringToVector(
        cmd.getString("platform-properties"), "platform-properties");
    options.d_environment =
        pairStringToVector(cmd.getString("environment"), "environment");

    options.d_execTimeout = cmd.getInt("exec-timeout");
    options.d_skipCacheLookup = cmd.getBool("skip-cache-lookup");
    options.d_doNotCache = cmd.getBool("do-not-cache");

    if (cmd.exists("salt")) {
        options.d_salt = cmd.getString("salt");
    }
    else {
        options.d_salt = "";
    }

    if (cmd.exists("priority")) {
        options.d_priority = std::make_shared<int>(cmd.getInt("priority"));
    }
    else {
        options.d_priority = nullptr;
    }

    if (cmd.exists("no-wait") && cmd.exists("wait")) {
        throw std::runtime_error("Requests cannot be both blocking and async");
    }
    options.d_blockingExecute = !cmd.exists("no-wait");

    options.d_operation =
        !cmd.exists("operation") ? "" : cmd.getString("operation");

    if (cmd.exists("result-metadata-file")) {
        options.d_resultMetadataFile = cmd.getString("result-metadata-file");
    }
    else {
        BUILDBOX_LOG_WARNING("result-metadata-file option is not set, "
                             "execution information might be lost")
    }

    // Tool name and version are set by CMAKE, and constructed using additional
    // input if provided
    options.d_metadata.insert(
        {"tool-name", cmd.exists("tool-name")
                          ? TREXE_METADATA_TOOL_NAME + std::string(":") +
                                cmd.getString("tool-name")
                          : TREXE_METADATA_TOOL_NAME});
    options.d_metadata.insert(
        {"tool-version", cmd.exists("tool-version")
                             ? TREXE_VERSION_EO + std::string(":") +
                                   cmd.getString("tool-version")
                             : TREXE_VERSION_EO});

    // "" is the proto default for string, so setting this here allows us to
    // treat the arguments as default if they aren't present, yet still send
    // the action_id
    options.d_metadata.insert(
        {"tool-invocation-id", cmd.exists("tool-invocation-id")
                                   ? cmd.getString("tool-invocation-id")
                                   : ""});
    options.d_metadata.insert({"correlated-invocations-id",
                               cmd.exists("correlated-invocations-id")
                                   ? cmd.getString("correlated-invocations-id")
                                   : ""});

    return options;
};

std::ostream &operator<<(std::ostream &os, ExecutionOptions eo)
{
    os << "ExecutionOptions:"
       << "Command=[";
    for (const auto &argv : eo.d_argv) {
        os << "'" << argv << "' ";
    }
    os << "]"
       << ", DownloadResults=[" << eo.d_downloadResultsPath << "]"
       << ", WorkingDirectory=[" << eo.d_workingDir << "]"
       << ", InputPaths=[";
    for (const auto &path : eo.d_inputPaths) {
        os << path << ";";
    }
    os << "]"
       << ", OutputPaths=[";
    for (const auto &path : eo.d_outputPaths) {
        os << path << ";";
    }
    os << "]"
       << ", Platform=[";
    for (const auto &pair : eo.d_platform) {
        os << "(" << pair.first << "," << pair.second << ");";
    }
    os << "]"
       << ", Environment=[";
    for (const auto &pair : eo.d_environment) {
        os << "(" << pair.first << "," << pair.second << ");";
    }
    os << "]"
       << ", BlockingExecute=[" << eo.d_blockingExecute << "]"
       << ", ExecutionTimeout=[" << eo.d_execTimeout << "]"
       << ", SkipCacheLookup=[" << eo.d_skipCacheLookup << "]"
       << ", DoNotCache=[" << !eo.d_doNotCache << "]";
    if (eo.d_priority != nullptr) {
        os << ", Priority=[" << *eo.d_priority << "]";
    }
    os << ", Metadata=[ "
       << "tool-name=[" << eo.d_metadata["tool-name"] << "], "
       << "tool-version=[" << eo.d_metadata["tool-version"] << "], "
       << "tool-invocation-id=[" << eo.d_metadata["tool-invocation-id"]
       << "], "
       << "correlated-invocations-id=["
       << eo.d_metadata["correlated-invocations-id"] << "], "
       << "GetCompletedOperation=[" << eo.d_operation << "]";

    return os;
};

} // namespace trexe
