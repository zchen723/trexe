/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>

#include <buildboxcommon_commandline.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_connectionoptions_commandline.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_remoteexecutionclient.h>

#include <trexe_actionbuilder.h>
#include <trexe_cmdlinespec.h>
#include <trexe_executioncontext.h>
#include <trexe_executionoptions.h>

using namespace trexe;
using namespace buildboxcommon;

int cancel(std::shared_ptr<const ExecutionOptions> execOptions,
           std::shared_ptr<const ConnectionOptions> connOptions,
           std::shared_ptr<const ConnectionOptions> connOptionsCAS,
           std::shared_ptr<const ConnectionOptions> connOptionsAC)
{
    auto executionContext = ExecutionContext(execOptions, connOptionsCAS,
                                             connOptions, connOptionsAC);

    return executionContext.cancelOperation() ? 0 : 1;
}

int execute(std::shared_ptr<const ExecutionOptions> execOptions,
            std::shared_ptr<const ConnectionOptions> connOptions,
            std::shared_ptr<const ConnectionOptions> connOptionsCAS,
            std::shared_ptr<const ConnectionOptions> connOptionsAC)
{

    // create execution context with provided options
    auto executionContext = ExecutionContext(execOptions, connOptionsCAS,
                                             connOptions, connOptionsAC);

    bool isResultCached = false;
    if (!execOptions->d_argv.size()) {
        BUILDBOX_LOG_DEBUG("Checking previously submitted async operation");
    } // Check action cache if applicable
    else if (execOptions->d_argv.size() &&
             !executionContext.skipsCacheLookup() &&
             (isResultCached = executionContext.isResultCached(true))) {
        BUILDBOX_LOG_DEBUG("Result was cached!");
    }
    else {
        // Submit execution
        // TODO nice error handling if for some reason this didn't work
        BUILDBOX_LOG_DEBUG("Will execute command: " << execOptions.get());
        if (executionContext.execute(!execOptions->d_blockingExecute) !=
            true) {
            BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                           "Execution submission failed.");
        }
        BUILDBOX_LOG_DEBUG("Execution service returned...");
    }

    // blocking vs async
    if (execOptions->d_argv.size() &&
        (execOptions->d_blockingExecute || isResultCached)) {
        std::shared_ptr<ActionResult> ar = executionContext.getActionResult();
        if (execOptions->d_downloadResultsPath.size()) {
            BUILDBOX_LOG_DEBUG(
                "Downloading results from blocking execution...");
            executionContext.downloadResults(
                *ar, execOptions->d_downloadResultsPath);
        }
        // Print stdout/err
        BUILDBOX_LOG_DEBUG("Command output follows:");
        std::cout << *executionContext.result_stdout(true);
        std::cerr << *executionContext.result_stderr(true);
        BUILDBOX_LOG_DEBUG("End of command output");

        int exit_code = ar->exit_code();
        BUILDBOX_LOG_DEBUG("Exit code was: " << exit_code);
        return exit_code;
    } // if async mode, there is nothing to download. The printed OperationId
      // can be used to fetch the status and results later

    if (!execOptions->d_operation.empty()) {
        if (execOptions->d_blockingExecute) {
            BUILDBOX_LOG_ERROR("Ignoring --operation, wait "
                               "mode not yet supported");
            return 1;
        }
        else if (execOptions->d_downloadResultsPath.empty()) {
            BUILDBOX_LOG_ERROR("Ignoring --operation, no "
                               "download path specified. Add a --d argument");
            return 1;
        }
        else {
            BUILDBOX_LOG_DEBUG(std::string("Downloading ") +
                               execOptions->d_operation + std::string(" to ") +
                               execOptions->d_downloadResultsPath);
            if (!executionContext.downloadCompletedOperation(
                    execOptions->d_downloadResultsPath)) {
                BUILDBOX_LOG_ERROR("Operation download failed");
                return 1;
            }

            // Print stdout/err of the async command
            BUILDBOX_LOG_DEBUG("Async command output follows:");
            std::cout << *executionContext.result_stdout(true);
            std::cerr << *executionContext.result_stderr(true);
            BUILDBOX_LOG_DEBUG("End of async command output");

            BUILDBOX_LOG_DEBUG("Operation download succeeded");
            std::shared_ptr<const ActionResult> actionResult =
                executionContext.actionResult();
            if (actionResult == nullptr) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::logic_error, "ActionResult cannot be null after "
                                      "downloading it successfully");
            }
            BUILDBOX_LOG_DEBUG("Exit code was: " << actionResult->exit_code());
            return actionResult->exit_code();
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    // Initialize logger
    buildboxcommon::logging::Logger::getLoggerInstance().initialize(argv[0]);

    // Connection Options objects
    CmdLineSpec trexeSpec(
        buildboxcommon::ConnectionOptionsCommandLine("", ""),
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-"),
        buildboxcommon::ConnectionOptionsCommandLine("ActionCache", "ac-"),
        buildboxcommon::ConnectionOptionsCommandLine("Execution", "exec-"));

    try {
        CommandLine commandLine(trexeSpec.d_spec);
        const bool success = commandLine.parse(argc, argv);
        if (!success || argc == 1) {
            commandLine.usage();
            return 1;
        }
        if (commandLine.exists("help")) {
            commandLine.usage();
            return 0;
        }

        // Set log level
        {
            const auto logLevelString =
                commandLine.getString("log-level").c_str();
            const auto logLevelMap = logging::stringToLogLevelMap();
            const auto logLevel = logLevelMap.find(logLevelString);
            if (logLevel != logLevelMap.end()) {
                BUILDBOX_LOG_SET_LEVEL(logLevel->second);
            }
            else {
                BUILDBOX_LOG_ERROR("Invalid log level: " << logLevelString);
                commandLine.usage();
                return 1;
            }
        }

        std::shared_ptr<const ExecutionOptions> execOptions =
            std::make_shared<const ExecutionOptions>(
                ExecutionOptions::fromCommandLine(trexeSpec.d_command,
                                                  commandLine));

        ConnectionOptions connectionOptions, connectionOptionsCAS,
            connectionOptionsAC, connectionOptionsEX;
        ConnectionOptionsCommandLine::configureChannel(commandLine, "",
                                                       &connectionOptions);

        // Allow different options per channel type (ac/cas/execution)
        // Configure an initial set of options settings for all channel
        // types, and then update accordingly with channel specific options
        connectionOptionsCAS = connectionOptions;
        connectionOptionsAC = connectionOptions;
        connectionOptionsEX = connectionOptions;

        ConnectionOptionsCommandLine::updateChannelOptions(
            commandLine, "cas-", &connectionOptionsCAS);
        ConnectionOptionsCommandLine::updateChannelOptions(
            commandLine, "ac-", &connectionOptionsAC);
        ConnectionOptionsCommandLine::updateChannelOptions(
            commandLine, "exec-", &connectionOptionsEX);

        // If --remote is not set, all the urls for the individual channel
        // types (cas/ac/exec) should bet set
        if (connectionOptions.d_url.empty()) {
            if (connectionOptionsCAS.d_url.empty() ||
                connectionOptionsAC.d_url.empty() ||
                connectionOptionsEX.d_url.empty()) {
                BUILDBOX_LOG_ERROR(
                    "Incorrect use of --remote connection options. "
                    "Use default --remote or set a value for each "
                    "channel type (cas/ac/exec).");
                return 1;
            }
        }
        if (execOptions->d_cancelMode) {
            return cancel(
                execOptions,
                std::make_shared<const ConnectionOptions>(connectionOptionsEX),
                std::make_shared<const ConnectionOptions>(
                    connectionOptionsCAS),
                std::make_shared<const ConnectionOptions>(
                    connectionOptionsAC));
        }
        else {
            return execute(
                execOptions,
                std::make_shared<const ConnectionOptions>(connectionOptionsEX),
                std::make_shared<const ConnectionOptions>(
                    connectionOptionsCAS),
                std::make_shared<const ConnectionOptions>(
                    connectionOptionsAC));
        }
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_ERROR("Error in trexe: " << e.what());
        return 1;
    }
}
