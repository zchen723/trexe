/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <trexe_executioncontext.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_grpcclient.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protos.h>
#include <fstream>
#include <google/protobuf/util/json_util.h>
#include <google/rpc/code.pb.h>

namespace trexe {

ExecutionContext::ExecutionContext(
    std::shared_ptr<const ExecutionOptions> options,
    std::shared_ptr<const ConnectionOptions> casConnectionOptions,
    std::shared_ptr<const ConnectionOptions> executionConnectionOptions,
    std::shared_ptr<const ConnectionOptions> actioncacheConnectionOptions)
    : d_executionOptions(options)
{
    // Initialize actionBuilder if applicable
    if (options->d_operation.empty()) {
        ActionBuilder actionBuilder =
            ActionBuilder(options->d_argv, options->d_workingDir,
                          options->d_inputPaths, options->d_outputPaths,
                          options->d_platform, options->d_environment,
                          options->d_execTimeout, options->d_doNotCache,
                          options->d_salt, options->d_outputNodeProperties);

        setActionData(actionBuilder.getActionData());
    }
    // Initialize CAS Client
    {
        auto grpcClient = std::make_shared<GrpcClient>();
        grpcClient->init(*casConnectionOptions);
        if (options->d_metadata.count("tool-name") &&
            options->d_metadata.count("tool-version")) {
            grpcClient->setToolDetails(options->d_metadata.at("tool-name"),
                                       options->d_metadata.at("tool-version"));
        }
        if (options->d_metadata.count("tool-invocation-id") &&
            options->d_metadata.count("correlated-invocations-id")) {
            grpcClient->setRequestMetadata(
                !options->d_operation.empty()
                    ? ""
                    : buildboxcommon::toString(*d_actionData.d_actionDigest),
                options->d_metadata.at("tool-invocation-id"),
                options->d_metadata.at("correlated-invocations-id"));
        }

        d_casClient = std::make_shared<CASClient>(grpcClient);
        d_casClient->init();
    }
    // Initialize RemoteExecution Client
    {
        auto executionGrpcClient = std::make_shared<GrpcClient>();
        executionGrpcClient->init(*executionConnectionOptions);
        if (options->d_metadata.count("tool-name") &&
            options->d_metadata.count("tool-version")) {
            executionGrpcClient->setToolDetails(
                options->d_metadata.at("tool-name"),
                options->d_metadata.at("tool-version"));
        }
        if (options->d_metadata.count("tool-invocation-id") &&
            options->d_metadata.count("correlated-invocations-id")) {
            executionGrpcClient->setRequestMetadata(
                !options->d_operation.empty()
                    ? ""
                    : buildboxcommon::toString(*d_actionData.d_actionDigest),
                options->d_metadata.at("tool-invocation-id"),
                options->d_metadata.at("correlated-invocations-id"));
        }

        auto actioncacheGrpcClient = std::make_shared<GrpcClient>();
        actioncacheGrpcClient->init(*actioncacheConnectionOptions);
        if (options->d_metadata.count("tool-name") &&
            options->d_metadata.count("tool-version")) {
            actioncacheGrpcClient->setToolDetails(
                options->d_metadata.at("tool-name"),
                options->d_metadata.at("tool-version"));
        }
        if (options->d_metadata.count("tool-invocation-id") &&
            options->d_metadata.count("correlated-invocations-id")) {
            actioncacheGrpcClient->setRequestMetadata(
                !options->d_operation.empty()
                    ? ""
                    : buildboxcommon::toString(*d_actionData.d_actionDigest),
                options->d_metadata.at("tool-invocation-id"),
                options->d_metadata.at("correlated-invocations-id"));
        }

        d_remoteExecutionClient = std::make_shared<RemoteExecutionClient>(
            executionGrpcClient, actioncacheGrpcClient);
        d_remoteExecutionClient->init();
    }
}

ExecutionContext::ExecutionContext(
    std::shared_ptr<const ExecutionOptions> options,
    std::shared_ptr<CASClient> casClient,
    std::shared_ptr<RemoteExecutionClient> reClient)
    : d_executionOptions(options)
{
    d_casClient = casClient;
    d_casClient->init();
    d_remoteExecutionClient = reClient;
    d_remoteExecutionClient->init();
}

ExecutionContext::~ExecutionContext()
{
    // When the ExecutionContext is out of the scope,
    // it writes the metadata file if user specifies the path
    if (!d_executionOptions->d_resultMetadataFile.empty()) {
        BUILDBOX_LOG_DEBUG("Writing result metadata to: "
                           << d_executionOptions->d_resultMetadataFile);
        writeResultMetadata(d_executionOptions->d_resultMetadataFile);
    }
}

void ExecutionContext::setActionData(const ActionData &actionData)
{
    d_actionData = actionData;
}

bool ExecutionContext::skipsCacheLookup() const
{
    return d_executionOptions->d_skipCacheLookup;
}

bool ExecutionContext::isResultCached(bool allowRemoteQuery)
{
    return (getActionResult(allowRemoteQuery) != nullptr);
}

bool ExecutionContext::downloadResults(const ActionResult &ar,
                                       const std::string where)
{
    if (where.size() == 0) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Download location is empty.");
    }

    buildboxcommon::FileUtils::createDirectory(where.c_str());
    buildboxcommon::FileDescriptor dirfd(
        open(where.c_str(), O_RDONLY | O_DIRECTORY));
    if (dirfd.get() < 0) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Download location "
                                           << where << " can't be opened.");
    }

    BUILDBOX_LOG_DEBUG("Output directory ready: " << where);

    try {
        d_remoteExecutionClient->downloadOutputs(d_casClient.get(), ar,
                                                 dirfd.get());
    }
    catch (buildboxcommon::GrpcError &grpcError) {
        updateGrpcError(grpcError.status);
        throw;
    }

    BUILDBOX_LOG_DEBUG("Results downloaded!");
    return true;
}

/**
 * Return the ActionResult for the given Operation. Throws an exception
 * if the Operation finished with an error, or if the Operation hasn't
 * finished yet.
 */
void ExecutionContext::getActionResult(const Operation &operation,
                                       ActionResult *ar)
{
    if (!operation.done()) {
        throw std::logic_error(
            "Called getActionResult on an unfinished Operation");
    }
    else if (operation.has_error()) {
        throw GrpcError("Operation failed: " + operation.error().message(),
                        grpc::Status(static_cast<grpc::StatusCode>(
                                         operation.error().code()),
                                     operation.error().message()));
    }
    else if (!operation.response().Is<ExecuteResponse>()) {
        throw std::runtime_error("Server returned invalid Operation result");
    }

    ExecuteResponse executeResponse;
    if (!operation.response().UnpackTo(&executeResponse)) {
        throw std::runtime_error("Operation response unpacking failed");
    }

    const auto executeStatus = executeResponse.status();
    if (executeStatus.code() != google::rpc::Code::OK) {
        throw GrpcError(
            "Execution failed: " + executeStatus.message(),
            grpc::Status(static_cast<grpc::StatusCode>(executeStatus.code()),
                         executeStatus.message()));
    }

    const ActionResult actionResult = executeResponse.result();
    if (actionResult.exit_code() == 0) {
        BUILDBOX_LOG_DEBUG("Execute response message: " +
                           executeResponse.message());
    }
    else if (!executeResponse.message().empty()) {
        BUILDBOX_LOG_INFO("Remote execution message: " +
                          executeResponse.message());
    }

    *ar = actionResult;
}

bool ExecutionContext::downloadCompletedOperation(const std::string &where)
{
    try {
        auto op = d_remoteExecutionClient->getOperation(
            d_executionOptions->d_operation);
        updateOperationMetadata(op);
        if (op.done()) {
            if (op.has_error()) {
                BUILDBOX_LOG_ERROR("Operation FAILED with error: " +
                                   op.error().DebugString());
                return false;
            }
            else {
                ActionResult ar;
                getActionResult(op, &ar);
                d_actionResult = std::make_shared<ActionResult>(ar);
                updateActionResultMetadata(ar);
                return downloadResults(ar, where);
            }
        }
        else {
            BUILDBOX_LOG_ERROR(std::string("Operation ") +
                               d_executionOptions->d_operation +
                               std::string(" not yet completed"));
            return false;
        }
    }
    catch (buildboxcommon::GrpcError &grpcError) {
        updateGrpcError(grpcError.status);
        throw;
    }
}

bool ExecutionContext::cancelOperation()
{
    BUILDBOX_LOG_DEBUG("Cancelling operation ["
                       << d_executionOptions->d_operation << "]");
    try {
        d_remoteExecutionClient->cancelOperation(
            d_executionOptions->d_operation);
    }
    catch (buildboxcommon::GrpcError &grpcError) {
        updateGrpcError(grpcError.status);
        throw;
    }
    return true;
}

bool ExecutionContext::execute(bool async)
{
    BUILDBOX_LOG_DEBUG("Execute called with async=" << async);
    // Upload missing blobs as necessary
    {
        BUILDBOX_LOG_DEBUG("Figuring out missing blobs to upload.");

        // Action and Command protos
        digest_string_map additionalProtosNeeded;
        additionalProtosNeeded.emplace(
            *d_actionData.d_actionDigest,
            d_actionData.d_actionProto->SerializeAsString());
        additionalProtosNeeded.emplace(
            *d_actionData.d_commandDigest,
            d_actionData.d_commandProto->SerializeAsString());

        std::vector<Digest> inputDigests;
        for (const auto &blob : *d_actionData.d_inputDigestsToPaths) {
            inputDigests.emplace_back(blob.first);
        }
        for (const auto &blob :
             *d_actionData.d_inputDigestsToSerializedProtos) {
            inputDigests.emplace_back(blob.first);
        }
        for (const auto &blob : additionalProtosNeeded) {
            inputDigests.emplace_back(blob.first);
        }
        BUILDBOX_LOG_DEBUG("Count of blobs needed: " << inputDigests.size());

        std::vector<Digest> missingDigests =
            d_casClient->findMissingBlobs(inputDigests);

        BUILDBOX_LOG_DEBUG(
            "Count of missing blobs: " << missingDigests.size());

        std::vector<buildboxcommon::CASClient::UploadRequest> upload_requests;
        upload_requests.reserve(missingDigests.size());
        for (const auto &digest : missingDigests) {
            if (d_actionData.d_inputDigestsToPaths->count(digest) == 1) {
                upload_requests.push_back(
                    buildboxcommon::CASClient::UploadRequest::from_path(
                        digest,
                        d_actionData.d_inputDigestsToPaths->at(digest)));
            }
            else if (d_actionData.d_inputDigestsToSerializedProtos->count(
                         digest)) {
                auto blob =
                    d_actionData.d_inputDigestsToSerializedProtos->at(digest);
                upload_requests.push_back(
                    buildboxcommon::CASClient::UploadRequest(digest, blob));
            }
            else if (additionalProtosNeeded.count(digest)) {
                auto blob = additionalProtosNeeded.at(digest);
                upload_requests.push_back(
                    buildboxcommon::CASClient::UploadRequest(digest, blob));
            }
            else {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error, "FMB reported unknown blobs digest=["
                                            << toString(digest)
                                            << "] missing!");
            }
        }

        BUILDBOX_LOG_DEBUG("Need to upload " << upload_requests.size()
                                             << " blobs");
        auto failedUploads = d_casClient->uploadBlobs(upload_requests);
        if (failedUploads.size() != 0) {
            updateGrpcError(failedUploads.at(0).status);
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error,
                "Failed to upload " << failedUploads.size() << " blobs.");
        }
    }

    bool stop_requested = false;
    ExecutionPolicy *policyPtr = nullptr;
    if (d_executionOptions->d_priority != nullptr) {
        ExecutionPolicy policy;
        policy.set_priority(*d_executionOptions->d_priority);
        policyPtr = &policy;
    }
    // Execute vs WaitExecute
    try {
        if (async) {
            google::longrunning::Operation op =
                d_remoteExecutionClient->asyncExecuteAction(
                    *d_actionData.d_actionDigest, stop_requested,
                    d_executionOptions->d_skipCacheLookup, policyPtr);
            updateOperationMetadata(op);
            std::cout << op.name() << std::endl;
        }
        else {
            d_actionResult = std::make_shared<ActionResult>(
                d_remoteExecutionClient->executeAction(
                    *d_actionData.d_actionDigest, stop_requested,
                    d_executionOptions->d_skipCacheLookup, policyPtr));
            updateActionResultMetadata(*d_actionResult);
            d_getActionResultRemoteQueryCalled =
                true; // implicitly got ActionResult
            BUILDBOX_LOG_DEBUG("ExecuteAction returned!");
        }
    }
    catch (const buildboxcommon::GrpcError &e) {
        BUILDBOX_LOG_ERROR("Error in ExecuteAction: "
                           << e.status.error_code() << ": "
                           << e.status.error_message());
        return false;
    }

    if (d_actionResult != nullptr ||
        async) { // if async return true, as the actionResult will not
                 // be finished
        return true;
    }
    return false;
}

std::shared_ptr<ActionResult>
ExecutionContext::getActionResult(bool allowRemoteQuery)
{
    if (!d_actionResult && !d_getActionResultRemoteQueryCalled) {
        if (!allowRemoteQuery) {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error, "Called getActionResult for the first "
                                    "time but not allowing remote query!");
        }
        ActionResult actionResult;
        std::set<std::string>
            outputsToInline; // we don't want anything to be inlined
        if (d_remoteExecutionClient->fetchFromActionCache(
                *d_actionData.d_actionDigest, outputsToInline,
                &actionResult)) {
            d_actionResult = std::make_shared<ActionResult>(actionResult);
            updateActionResultMetadata(actionResult);
        }
        d_getActionResultRemoteQueryCalled = true;
    }

    return d_actionResult;
}

std::shared_ptr<std::string>
ExecutionContext::result_stdout(bool allowRemoteQuery)
{
    if (!d_stdout) {
        if (!d_actionResult) {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error, "ActionResult not set, can't get stdout.");
        }
        // The raw data defaults to empty string when not set by the server.
        else if (d_actionResult->stdout_raw().length() > 0) {
            d_stdout =
                std::make_shared<std::string>(d_actionResult->stdout_raw());
        }
        else if (d_actionResult->has_stdout_digest()) {
            if (!allowRemoteQuery) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error,
                    "Called stdout() without allowing remote query for the "
                    "first time, can't get stdout from CAS.");
            }
            d_stdout = std::make_shared<std::string>(
                d_casClient->fetchString(d_actionResult->stdout_digest()));
        }
        else {
            BUILDBOX_LOG_DEBUG("No inlined or digest-based stdout "
                               << "was provided by the remote service. "
                               << "Defaulting to empty string.")
            d_stdout = std::make_shared<std::string>("");
        }
    }
    return d_stdout;
}

std::shared_ptr<std::string>
ExecutionContext::result_stderr(bool allowRemoteQuery)
{
    if (!d_stderr) {
        if (!d_actionResult) {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error, "ActionResult not set, can't get stderr.");
        }
        // The raw data defaults to empty string when not set by the server.
        else if (d_actionResult->stderr_raw().length() > 0) {
            d_stderr =
                std::make_shared<std::string>(d_actionResult->stderr_raw());
        }
        else if (d_actionResult->has_stderr_digest()) {
            if (!allowRemoteQuery) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error,
                    "Called stderr() without allowing remote query for the "
                    "first time, can't get stderr from CAS.");
            }
            d_stderr = std::make_shared<std::string>(
                d_casClient->fetchString(d_actionResult->stderr_digest()));
        }
        else {
            BUILDBOX_LOG_DEBUG("No inlined or digest-based stderr "
                               << "was provided by the remote service. "
                               << "Defaulting to empty string.")
            d_stderr = std::make_shared<std::string>("");
        }
    }
    return d_stderr;
}

void ExecutionContext::writeResultMetadata(const std::string &filepath)
{
    std::ofstream resultMetadataFile(filepath,
                                     std::fstream::out | std::fstream::trunc);
    if (!resultMetadataFile.good()) {
        BUILDBOX_LOG_ERROR("cannot write to result metadata file "
                           << filepath);
        return;
    }
    try {
        google::protobuf::util::JsonPrintOptions printOptions;
        printOptions.always_print_primitive_fields = true;

        std::string resultMetadataJson;
        google::protobuf::util::MessageToJsonString(
            d_resultMetadata, &resultMetadataJson, printOptions);

        resultMetadataFile << resultMetadataJson;
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_ERROR(
            "cannot deserialize result metadata to output file: " << filepath);
    }
}

void ExecutionContext::updateActionResultMetadata(
    const ActionResult &actionResult)
{
    d_resultMetadata.mutable_action_result_metadata()->set_exit_code(
        actionResult.exit_code());
}

void ExecutionContext::updateOperationMetadata(const Operation &operation)
{
    d_resultMetadata.mutable_operation_metadata()->set_name(operation.name());
    if (operation.has_error()) {
        *d_resultMetadata.mutable_operation_metadata()
             ->mutable_operation_error() = operation.error();
    }
}

std::shared_ptr<const ActionResult> ExecutionContext::actionResult() const
{
    return d_actionResult;
}

void ExecutionContext::updateGrpcError(const grpc::Status &error)
{
    auto metadataError = d_resultMetadata.mutable_error();
    metadataError->set_code(error.error_code());
    *metadataError->mutable_message() = error.error_message();
}

const TrexeResultMetadata &ExecutionContext::resultMetadata() const
{
    return d_resultMetadata;
}
} // namespace trexe
