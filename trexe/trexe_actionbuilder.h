/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_TREXE_ACTIONBUILDER
#define INCLUDED_TREXE_ACTIONBUILDER

#include <map>
#include <memory>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_merklize.h>
#include <buildboxcommon_protos.h>

#include <trexe_actiondata.h>

using namespace buildboxcommon;

namespace trexe {

class ActionBuilder {
  public:
    ActionBuilder() = default; // included for member initialization only
    ActionBuilder(
        const std::vector<std::string> &argv, const std::string &cwd,
        const std::set<std::string> &inputs,
        const std::set<std::string> &outputs,
        const std::vector<std::pair<std::string, std::string>> &platform,
        const std::vector<std::pair<std::string, std::string>> &environment,
        const int &execTimeout, const bool &doNotCache,
        const std::string &salt,
        const std::set<std::string> &outputNodeProperties);

    std::pair<std::shared_ptr<Digest>, std::shared_ptr<Command>>
    generateCommand();
    std::pair<std::shared_ptr<Digest>, std::shared_ptr<Action>>
    generateAction();
    bool generateInputMerkleTree();

    std::shared_ptr<const Digest> actionDigest() const;
    std::shared_ptr<const Action> actionProto() const;
    std::string actionDigestString() const;
    std::shared_ptr<const digest_string_map> inputDigestsToPaths() const;
    std::shared_ptr<const digest_string_map>
    inputDigestsToSerializedProtos() const;
    std::shared_ptr<const Digest> commandDigest() const;
    std::shared_ptr<const Command> commandProto() const;
    ActionData getActionData();

  protected:
    // Alternative constructors useful for testing
    ActionBuilder(
        bool init, const std::vector<std::string> &argv,
        const std::string &cwd, const std::set<std::string> &inputs,
        const std::set<std::string> &outputs,
        const std::vector<std::pair<std::string, std::string>> &platform,
        const std::vector<std::pair<std::string, std::string>> &environment,
        const int &execTimeout, const bool &doNotCache,
        const std::string &salt,
        const std::set<std::string> &outputNodeProperties);

    void init_protos();

    // Command
    std::vector<std::string> d_argv;
    std::string d_workingDir;
    std::set<std::string> d_outputPaths;
    std::vector<std::pair<std::string, std::string>> d_environment;
    std::vector<std::pair<std::string, std::string>> d_platform;
    std::set<std::string> d_outputNodeProperties;

    // Action
    std::set<std::string> d_inputPaths;
    int d_execTimeout;
    bool d_doNotCache;
    std::string d_salt;

    // Generated
    std::shared_ptr<Command> d_commandProto;
    std::shared_ptr<Digest> d_commandDigest;
    std::shared_ptr<Action> d_actionProto;
    std::shared_ptr<Digest> d_actionDigest;
    std::shared_ptr<Digest> d_inputRootDigest;
    NestedDirectory d_inputRoot;
    std::shared_ptr<digest_string_map> d_inputDigestsToPaths;
    std::shared_ptr<digest_string_map> d_inputDigestsToSerializedProtos;

    // Generates input tree and returns everything needed for
    // FMB and uploads
    // namely:
    // - tree digest
    // - NestedDirectory struct
    // - digestToDiskPath (all needed file digests to path map)
    // - digestToDirMessages (all needed dir proto digests to serialized proto
    // map)
    std::tuple<std::shared_ptr<Digest>, NestedDirectory,
               std::shared_ptr<digest_string_map>,
               std::shared_ptr<digest_string_map>>
    generateInputTree();

  private:
};

} // namespace trexe

#endif
