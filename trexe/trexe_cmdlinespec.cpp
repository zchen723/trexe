/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <sstream>

#include <buildboxcommon_logging.h>
#include <trexe_cmdlinespec.h>

namespace trexe {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

CmdLineSpec::CmdLineSpec(
    buildboxcommon::ConnectionOptionsCommandLine const &connectionOptionsSpec,
    buildboxcommon::ConnectionOptionsCommandLine const
        &connectionOptionsCasSpec,
    buildboxcommon::ConnectionOptionsCommandLine const
        &connectionOptionsAcSpec,
    buildboxcommon::ConnectionOptionsCommandLine const
        &connectionOptionsExecSpec)
{
    d_spec.emplace_back("help", "Display usage and exit.",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));

    d_spec.insert(d_spec.end(), connectionOptionsSpec.spec().cbegin(),
                  connectionOptionsSpec.spec().cend());

    d_spec.insert(d_spec.end(), connectionOptionsCasSpec.spec().cbegin(),
                  connectionOptionsCasSpec.spec().cend());

    d_spec.insert(d_spec.end(), connectionOptionsAcSpec.spec().cbegin(),
                  connectionOptionsAcSpec.spec().cend());

    d_spec.insert(d_spec.end(), connectionOptionsExecSpec.spec().cbegin(),
                  connectionOptionsExecSpec.spec().cend());

    d_spec.emplace_back("d",
                        "Download completed action outputs to this path. "
                        "Leave empty for no download.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue(""));

    d_spec.emplace_back("working-dir", "Path to working directory.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("."));

    d_spec.emplace_back("input-path",
                        "Input path for Action. "
                        "--input-path=<path> for each input",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("output-path",
                        "Output path to capture. "
                        "--output-path=<path> for each output",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("output-node-properties",
                        "A list of keys that indicate what additional file "
                        "attributes should be captured "
                        "--output-node-properties=<name> of each property",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "platform-properties",
        "The platform requirements for the execution environment "
        "--platform-properties=<key>=<value>,<key>=... for each property",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(""));

    d_spec.emplace_back(
        "environment",
        "Environment variables to set in the running program's environment "
        "--environment=<key>=<value>,<key>=... for each variable to set",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(""));

    d_spec.emplace_back(
        "exec-timeout",
        "The timeout after which the execution of an Action should be killed "
        "--exec-timeout=<seconds>",
        TypeInfo(DataType::COMMANDLINE_DT_INT), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(0));

    d_spec.emplace_back("priority",
                        "The priority (relative importance) of this action. A "
                        "priority of 0 means the *default* priority "
                        "--priority=<priority>",
                        TypeInfo(DataType::COMMANDLINE_DT_INT),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("skip-cache-lookup",
                        "Flag indicating that the server should not check the "
                        "cache when executing an action",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITHOUT_ARG,
                        DefaultValue(false));

    d_spec.emplace_back(
        "do-not-cache",
        "Flag indicating that ActionResults should not be cached, and "
        "duplicate Action executions will not be merged",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG, DefaultValue(false));

    d_spec.emplace_back(
        "correlated-invocations-id",
        "An identifier to tie multiple tool invocations together. "
        "For example, runs of foo_test, bar_test and baz_test on a "
        "post-submit of a given patch. "
        "--correlated-invocations-id=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "tool-invocation-id",
        "An identifier that ties multiple actions together to a "
        "final result. For example, multiple actions are required"
        " to build and run foo_test. "
        "--tool-invocation-id=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "tool-name",
        "Additional string to append to the tool-name (trexe) sent in the "
        "metadata. Useful to append additional information about "
        "invocation e.x. python to indicate python was used"
        "--tool-name=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "tool-version",
        "Additional string to append to the tool-version (e.x. 0.0.1) sent "
        "in the metadata. Useful to append additional information about "
        "invocation e.x. 2.3.4 to append 2.3.4 (version of a package named in"
        "tool-name) to the tool-version metadata"
        "--tool-version=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "salt",
        "An optional value to place the `Action` into a separate cache "
        "namespace from other instances having the same field contents"
        "--salt=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "wait",
        "Flag indicating that calls from trexe should be blocking."
        "this is the default behavior",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back(
        "no-wait",
        "Flag indicating that calls from trexe should be async. This"
        "will cause trexe to exit with code 0 upon successful"
        " job submission and write the operation id to `stdout` "
        "in plaintext (e.g. app-cr/6283a9c6-85b2-4b03-92bd)",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back(
        "operation",
        "Operation ID to lookup and download outputs from (to the "
        " specified directory) if -d is set.",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "cancel", "Cancels the specified operation. Requires --operation.",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back("result-metadata-file",
                        "The metadata of trexe execution in JSON will be "
                        "written into this file",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    {
        std::stringstream logLevels;
        logLevels << "(";
        bool first = true;
        for (const auto &level :
             buildboxcommon::logging::stringToLogLevelMap()) {
            logLevels << (first ? "" : ", ") << level.first;
            first = false;
        }
        logLevels << ")";
        d_spec.emplace_back("log-level", "Log level " + logLevels.str(),
                            TypeInfo(DataType::COMMANDLINE_DT_STRING),
                            ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                            DefaultValue({"debug"}));
    }

    d_spec.emplace_back("", "Command to remote", TypeInfo(&d_command),
                        ArgumentSpec::O_OPTIONAL,
                        ArgumentSpec::C_WITH_REST_OF_ARGS);
}

}; // namespace trexe
