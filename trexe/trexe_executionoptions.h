/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_TREXE_EXECUTIONOPTIONS
#define INCLUDED_TREXE_EXECUTIONOPTIONS

#include <memory>
#include <ostream>
#include <set>
#include <string>
#include <unordered_map>

#include <buildboxcommon_commandline.h>
#include <trexe_cmdlinespec.h>

namespace trexe {

struct ExecutionOptions {
    bool d_cancelMode = false;
    bool d_blockingExecute = true;
    bool d_skipCacheLookup;
    bool d_doNotCache;
    std::string d_downloadResultsPath; // do not download when empty
    std::vector<std::string> d_argv;
    std::string d_workingDir;
    std::set<std::string> d_inputPaths;
    std::set<std::string> d_outputPaths;
    std::set<std::string> d_outputNodeProperties;
    std::string d_operation;
    std::vector<std::pair<std::string, std::string>> d_platform;
    std::vector<std::pair<std::string, std::string>> d_environment;
    int d_execTimeout;
    std::string d_salt;
    std::shared_ptr<int> d_priority;
    std::unordered_map<std::string, std::string> d_metadata;
    std::string d_resultMetadataFile;

    static ExecutionOptions
    fromCommandLine(const std::vector<std::string> &,
                    const buildboxcommon::CommandLine &);

    friend std::ostream &operator<<(std::ostream &, ExecutionOptions);
};

} // namespace trexe

#endif
