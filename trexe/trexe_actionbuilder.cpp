/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <trexe_actionbuilder.h>

#include <buildboxcommon_logging.h>
#include <buildboxcommon_mergeutil.h>

namespace trexe {

ActionBuilder::ActionBuilder(
    const std::vector<std::string> &argv, const std::string &cwd,
    const std::set<std::string> &inputPaths,
    const std::set<std::string> &outputPaths,
    const std::vector<std::pair<std::string, std::string>> &platform,
    const std::vector<std::pair<std::string, std::string>> &environment,
    const int &execTimeout, const bool &doNotCache, const std::string &salt,
    const std::set<std::string> &outputNodeProperties)
    : ActionBuilder(true, argv, cwd, inputPaths, outputPaths, platform,
                    environment, execTimeout, doNotCache, salt,
                    outputNodeProperties)
{
}

ActionBuilder::ActionBuilder(
    bool init, const std::vector<std::string> &argv, const std::string &cwd,
    const std::set<std::string> &inputPaths,
    const std::set<std::string> &outputPaths,
    const std::vector<std::pair<std::string, std::string>> &platform,
    const std::vector<std::pair<std::string, std::string>> &environment,
    const int &execTimeout, const bool &doNotCache, const std::string &salt,
    const std::set<std::string> &outputNodeProperties)
    : d_argv(argv), d_workingDir(cwd), d_inputPaths(inputPaths),
      d_outputPaths(outputPaths), d_platform(platform),
      d_environment(environment), d_execTimeout(execTimeout),
      d_doNotCache(doNotCache), d_salt(salt),
      d_outputNodeProperties(outputNodeProperties)
{
    if (init) {
        init_protos();
    }
}

void ActionBuilder::init_protos()
{
    {
        auto commandPair = generateCommand();
        d_commandDigest = commandPair.first;
        d_commandProto = commandPair.second;
    }
    BUILDBOX_LOG_DEBUG("Generated Command: Digest:"
                       << toString(*d_commandDigest)
                       << ", Command:" << d_commandProto->SerializeAsString());

    {
        auto inputTree = generateInputTree();
        d_inputRootDigest = std::get<0>(inputTree);
        d_inputRoot = std::move(std::get<1>(inputTree));
        d_inputDigestsToPaths = std::get<2>(inputTree);
        d_inputDigestsToSerializedProtos = std::get<3>(inputTree);
    }

    // For multiple input paths the NestedDirectory d_inputRoot only contains
    // the last processed --input-path, not the entire tree
    if (d_inputPaths.size() == 1) {
        BUILDBOX_LOG_DEBUG("Generated Command Input tree: " << d_inputRoot);
    }

    {
        auto actionPair = generateAction();
        d_actionDigest = actionPair.first;
        d_actionProto = actionPair.second;
    }
    BUILDBOX_LOG_DEBUG("Generated action: Digest: "
                       << toString(*d_actionDigest)
                       << ", Action:" << d_actionProto->SerializeAsString());
}

std::pair<std::shared_ptr<Digest>, std::shared_ptr<Command>>
ActionBuilder::generateCommand()
{
    BUILDBOX_LOG_DEBUG("Generating command.");
    auto command = std::make_shared<Command>();
    for (const auto &arg : d_argv) {
        command->add_arguments(arg);
    }

    // environment variables
    for (const auto &var : d_environment) {
        auto envVar = command->add_environment_variables();
        envVar->set_name(var.first);
        envVar->set_value(var.second);
    }

    for (const auto &path : d_outputPaths) {
        command->add_output_paths(path);
    }

    // platform (deprecated but set to the same value as the Action for
    // redundancy)
    Platform *commandPlatform = command->mutable_platform();
    for (const auto &p : d_platform) {
        auto property = commandPlatform->add_properties();
        property->set_name(p.first);
        property->set_value(p.second);
    }

    command->set_working_directory(d_workingDir);

    // ouptut_node_properties
    for (const auto &property : d_outputNodeProperties) {
        command->add_output_node_properties(property);
    }

    auto digest =
        std::make_shared<Digest>(CASHash::hash(command->SerializeAsString()));
    return std::make_pair<>(digest, command);
}

std::pair<std::shared_ptr<Digest>, std::shared_ptr<Action>>
ActionBuilder::generateAction()
{
    BUILDBOX_LOG_DEBUG("Generating Action: ");
    // Generate command
    if (d_commandProto == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Command proto not generated.");
    }

    if (d_commandDigest == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Command digest not generated.");
    }

    auto action = std::make_shared<Action>();
    action->mutable_command_digest()->CopyFrom(*d_commandDigest);

    // input_root_digest
    action->mutable_input_root_digest()->CopyFrom(*d_inputRootDigest);

    // timeout
    if (d_execTimeout != 0) { // optional, has server default if not specified
        google::protobuf::Duration actionTimeout;
        actionTimeout.set_seconds(d_execTimeout);
        action->mutable_timeout()->CopyFrom(actionTimeout);
    }

    // do_not_cache
    action->set_do_not_cache(d_doNotCache);

    // salt
    if (d_salt != "") {
        action->set_salt(d_salt);
    }

    // platform
    Platform *actionPlatform = action->mutable_platform();
    for (const auto &p : d_platform) {
        auto property = actionPlatform->add_properties();
        property->set_name(p.first);
        property->set_value(p.second);
    }

    auto digest =
        std::make_shared<Digest>(CASHash::hash(action->SerializeAsString()));

    return std::make_pair<>(digest, action);
}

std::tuple<std::shared_ptr<Digest>, NestedDirectory,
           std::shared_ptr<digest_string_map>,
           std::shared_ptr<digest_string_map>>
ActionBuilder::generateInputTree()
{
    BUILDBOX_LOG_DEBUG("Generating input merkle tree.");

    std::shared_ptr<std::unordered_map<buildboxcommon::Digest, std::string>>
        digestToDiskPath = std::make_shared<
            std::unordered_map<buildboxcommon::Digest, std::string>>();

    std::shared_ptr<digest_string_map> digestToSerializedProtos =
        std::make_shared<
            std::unordered_map<buildboxcommon::Digest, std::string>>();

    NestedDirectory inputNestedDir;
    Digest rootDigest;
    std::vector<std::vector<Directory>> treesToMerge;
    MergeUtil::DigestVector mergedDirList;

    if (d_inputPaths.size()) {
        for (const auto inputPath : d_inputPaths) {
            const char *path = inputPath.c_str();
            inputNestedDir =
                make_nesteddirectory(path, digestToDiskPath.get(), true);
            Tree rootTree = inputNestedDir.to_tree();
            std::vector<Directory> treeDirectory;
            treeDirectory.push_back(rootTree.root());
            for (const Directory child : rootTree.children()) {
                treeDirectory.push_back(child);
            }
            treesToMerge.push_back(treeDirectory);
        }

        const bool result = MergeUtil::createMergedDigest(
            treesToMerge, &rootDigest, digestToSerializedProtos.get(),
            &mergedDirList);
        if (!result) {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error,
                "Unable to merge input-path trees because of collision");
        }

        return std::make_tuple(std::make_shared<Digest>(rootDigest),
                               std::move(inputNestedDir), digestToDiskPath,
                               digestToSerializedProtos);
    }
    else {
        auto emptyDigest = std::make_shared<Digest>(
            inputNestedDir.to_digest(digestToSerializedProtos.get()));

        return std::make_tuple(emptyDigest, std::move(inputNestedDir),
                               digestToDiskPath, digestToSerializedProtos);
    }
}

std::shared_ptr<const Digest> ActionBuilder::actionDigest() const
{
    if (d_actionDigest == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Action digest not generated.");
    }
    return d_actionDigest;
}

std::shared_ptr<const Action> ActionBuilder::actionProto() const
{
    if (d_actionProto == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Action proto not generated.");
    }
    return d_actionProto;
}

std::shared_ptr<const Digest> ActionBuilder::commandDigest() const
{
    if (d_commandDigest == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Command digest not generated.");
    }
    return d_commandDigest;
}

std::shared_ptr<const Command> ActionBuilder::commandProto() const
{
    if (d_commandProto == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Command proto not generated.");
    }
    return d_commandProto;
}

std::string ActionBuilder::actionDigestString() const
{
    if (d_actionDigest == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Action digest not generated.");
    }
    return toString(*d_actionDigest);
}

std::shared_ptr<const digest_string_map>
ActionBuilder::inputDigestsToPaths() const
{
    if (d_inputDigestsToPaths == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Input digests to paths not generated.");
    }
    return d_inputDigestsToPaths;
}

std::shared_ptr<const digest_string_map>
ActionBuilder::inputDigestsToSerializedProtos() const
{
    if (d_inputDigestsToSerializedProtos == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Input digests to serialized protos not generated.");
    }
    return d_inputDigestsToSerializedProtos;
}

ActionData ActionBuilder::getActionData()
{
    ActionData data;
    data.d_actionDigest = actionDigest();
    data.d_actionProto = actionProto();
    data.d_commandDigest = commandDigest();
    data.d_commandProto = commandProto();
    data.d_inputDigestsToPaths = inputDigestsToPaths();
    data.d_inputDigestsToSerializedProtos = inputDigestsToSerializedProtos();
    return data;
}

} // namespace trexe
